

/*
 * BBTREE - Implementacija Anderssonovega drevesa Podana je samo podatkovna struktura
 * 			brez metod, saj se pojavi te�ava pri implementaciji. Pri metodi ADD in DELETE je
 * 			potrebno spremeniti objekt iz bottom v nek nov objekt, za kar nisem na�el kake
 * 			pametne re�itve. Namesto tega so v razredu BBTreeMain implementirane stati�ne metode,
 * 			kjer enostavno ustvarimo nov objekt in ga zamenjamo.
 */

public class BBTree {
	
	
	
	private Integer key;
	private Integer level;
	private BBTree left, right;
	
	
	public BBTree() {
		
	}
	
	
	public BBTree(Integer key, Integer level, BBTree left, BBTree right) {
		this.key = key;
		this.level = level;
		this.left = left;
		this.right = right;
	}
	
	


	public Integer getKey() {
		return key;
	}


	public void setKey(int key) {
		this.key = key;
	}


	public Integer getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public BBTree getLeft() {
		return left;
	}


	public void setLeft(BBTree left) {
		this.left = left;
	}


	public BBTree getRight() {
		return right;
	}


	public void setRight(BBTree right) {
		this.right = right;
	}
	
	
	
	
	
	
	/*
	public void skew() {
		
		BBTree temp;
		if (this.left.level == this.level) {
			temp = this;
			
			this.key = this.left.key;
			this.level = this.left.level;
			this.left = this.left.left;
			this.right = this.left.right;
	
			temp.left = this.right;
			this.right = temp;
		}
		
	}
	
	
	
	public void split() {
		
		BBTree temp;
		if (this.right.right.level == this.level) {
			temp = this;
			
			this.key = this.right.key;
			this.level = this.right.level;
			this.left = this.right.left;
			this.right = this.right.right;
			
			temp.right = this.left;
			this.left = temp;
			this.level += 1;
		}
		
	}
	
	
	
	
	public void add(int x, boolean ok) {
		
		
		if (this.equals(GlobalVariables.bottom)) {
			
			this.key = x;
			this.level = 1;
			this.left = GlobalVariables.bottom;
			this.right = GlobalVariables.bottom;
			ok = true;
		}
		
		else {
			if (x < this.getKey()) {
				this.left.add(x, ok);
			}
			else if (x > this.getKey()){
				this.right.add(x, ok);
			}
			//Element je �e notri
			else {
				ok = false;
			}
			this.skew();
			this.split();
		}
		
	}
	
	
	*/
	
	

}





