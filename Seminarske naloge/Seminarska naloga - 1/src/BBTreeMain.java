


/*
 * BBTreeMain - Razred, kjer so implementirane metode za operacije na drevesu. Metode
 * so stati�ne in po vsaki operaciji vrnejo novo drevo
 */


import java.util.TreeSet;

public class BBTreeMain {
	
	
	//Definiramo globalne spremenljivke, potrebne v implementaciji
	public static BBTree bottom, deleted, last;
	
	
	
	
	//Metoda za initializacijo globalnih spremenljivk
	public static void initGlobalVariables() {
		bottom = new BBTree();
		bottom.setLevel(0);
		bottom.setLeft(bottom);
		bottom.setRight(bottom);
		deleted = bottom;
	}
	
	
	
	
	//Implementacija skew iz �lanka
	public static BBTree skew(BBTree t) {
		
		BBTree temp;
		if (t.getLeft().getLevel() == t.getLevel()) {
			temp = t;
			t = t.getLeft();
			temp.setLeft(t.getRight());
			t.setRight(temp);
		}
		
		return t;
	}
	
	
	
	//Implementacija split iz �lanka
	public static BBTree split(BBTree t) {
		
		BBTree temp;
		if (t.getRight().getRight().getLevel() == t.getLevel()) {
			temp = t;
			t = t.getRight();
			temp.setRight(t.getLeft());
			t.setLeft(temp);
			t.setLevel(t.getLevel()+1);
		}
		
		return t;
	}
	
	
	
	
	//Implementacija dodajanja elementa iz drevesa
	public static BBTree add(Integer x, BBTree t) {
		
		
		BBTree res = t;
		// �e je bottom, ustavimo novo vozli��e in dodamo notri element
		if (t.equals(bottom)) {
			res = new BBTree(x, 1, bottom, bottom);
		}
		
		else {
			//�e je element manj�i, gremo v levo poddrevo in rezultatu nastavimo levi del
			if (x < t.getKey()) {
				BBTree leftSide = add(x, t.getLeft());
				res.setLeft(leftSide);
			}
			
			//Element je ve�ji, gremo v desno poddrevo in rezultatu nastavimo desni del
			else if (x > t.getKey()) {
				BBTree rightSide = add(x, t.getRight());
				res.setRight(rightSide);
			}
			// Sicer je element je �e notri in ne naredimo ni�
			
			//Izvedemo skew in spit, da ohranimo uravnote�enost drevesa
			res = skew(res);
			res = split(res);
		}
		return res;
		
	}
	
	
	
	
	//Implementacija odstranitve elementa iz drevesa
	public static BBTree remove(Integer x, BBTree t) {
		
		BBTree res = t;
		
		//�e je t==bottom, nimamo kaj brisati
		if (!t.equals(bottom)) {
			
			//Gremo dol po drevesu in nastavimo pointerje last in deleted (glede na velikost elementa gremo v levo ali desno poddrevo
			last = t;
			if (x < t.getKey()) {
				BBTree leftSide = remove(x, t.getLeft());
				res.setLeft(leftSide);
			}
			else {
				deleted = t;
				BBTree rightSide = remove(x, t.getRight());
				res.setRight(rightSide);
			}
			
			
			//Na dnu drevesa odstranimo element, �e je prisoten
			if (res.equals(last) && !deleted.equals(bottom) && x==deleted.getKey()) {
				deleted.setKey(res.getKey());
				deleted = bottom;
				res = res.getRight();
				last = null;
			}
			
			//Na poti nazaj poravnamo drevo
			else if ((res.getLeft().getLevel() < res.getLevel()-1) || (res.getRight().getLevel() < res.getLevel()-1)) {
				res.setLevel(res.getLevel()-1);
				if (res.getRight().getLevel() > res.getLevel()) {
					res.getRight().setLevel(res.getLevel());
				}
				
				//Izvedemo vse potrebne skew in res da ohranimo uravnote�enost drevesa
				res = skew(res);
				
				BBTree rightSide = skew(res.getRight());
				res.setRight(rightSide);
				
				BBTree rightRightSide = res.getRight().getRight();
				res.getRight().setRight(rightRightSide);
				
				res = split(res);
				
				BBTree rightSide2 = split(res.getRight());
				res.setRight(rightSide2);
				
			}
			
		}
		return res;
		
		
	}
	
	
	
	//CLEAR - za brisanje drevesa. Celotno drevo enostavno nastavimo na bottom
	public static BBTree clear(BBTree t) {
		return bottom;
	}
	
	
	
	
	//CONTAINS - preveri, �e se element x nahaja v drevesu t
	public static boolean contains(Integer x, BBTree t) {
		
		//Smo ga na�li, true
		if (t.getKey() == x) {
			return true;
		}
		
		//Pri�li smo do konca, elementa ni, false
		else if (t.equals(bottom)) {
			return false;
		}
		
		//Preverimo v poddrevesih
		else {
			
			//�e je manj�i od trenutnega, preverimo v levem poddrevesu
			if (x < t.getKey()) {
				return contains(x, t.getLeft());
			}
			
			//Ve�ji je od trenutnega, preverimo naprej v desnem poddrevesu
			else {
				return contains(x, t.getRight());
			}
		}
	}
	
	
	
	//FLOOR - implementirana metoda floor za na�e drevo
	public static Integer floor(Integer x, BBTree t) {
		
		//�e smo element na�li, potem je to �e rezultat
		if (t.getKey() == x) {
			return x;
		}
		
		//�e smo pri�li do konca, potem nismo na�li ustrezen rezulat, vrnemo null
		else if (t.equals(bottom)) {
			return null;
		}
		
		//Element je manj�i - gledamo levo poddrevo
		else if (x < t.getKey()) {
			return floor(x, t.getLeft());
		}
		
		//Element je ve�ji - gledamo desno poddrevo in za rezultat vzamemo maksimum od trenutnega elementa in elementa, dobimo iz desnega poddrevesa
		else {
			Integer floorRight = floor(x, t.getRight());
			
			//Poseben primer, �e smo pri�li do konca, ni� ne primerjamo
			if(floorRight == null) {
				return t.getKey();
			}
			else {
				return Math.max(t.getKey(), floorRight);
			}
		}
	}
 	
	
	
	//Pomo�na metoda za izpis drevesa
	public static void printTree(BBTree t) {
		
		if(t.equals(bottom)) {

		}
		else {
			System.out.print(t.getKey()+ "-level:"+t.getLevel() + " ");
			System.out.print("L(");
			printTree(t.getLeft());
			System.out.print(") ");
			System.out.print("R(");
			printTree(t.getRight());
			System.out.print(")");
		}
		
	}
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		
		
		
		initGlobalVariables();
		
		/*
		BBTree tree = new BBTree(5, 3, new BBTree(3, 2, new BBTree(1, 1, bottom, bottom), new BBTree(4, 1, bottom, bottom)), new BBTree(8, 2, new BBTree(7, 1, bottom, bottom), bottom));
		printTree(tree);
		
		System.out.println();
		System.out.println();
		
		BBTree treeSkew = skew(tree);
		printTree(treeSkew);
		
		System.out.println();
		System.out.println();
		
		
		
		
		BBTree testTree = new BBTree(5, 1, GlobalVariables.bottom, GlobalVariables.bottom);
		testTree.add(3, true);
		testTree.add(8, true);
		printTree(testTree);
		
		
		BBTree testTree = bottom;
		testTree = add(10, testTree);
		testTree = add(8, testTree);
		testTree = add(1, testTree);
		testTree = add(4, testTree);
		testTree = add(7, testTree);
		testTree = add(2, testTree);
		printTree(testTree);
		
		System.out.println();
		System.out.println();
		
		
		BBTree testTreeDelete = bottom;
		testTreeDelete = add(10, testTreeDelete);
		testTreeDelete = add(8, testTreeDelete);
		testTreeDelete = add(1, testTreeDelete);
		testTreeDelete = add(4, testTreeDelete);
		testTreeDelete = add(7, testTreeDelete);
		testTreeDelete = add(2, testTreeDelete);
		
		testTreeDelete = remove(3, testTreeDelete);
		testTreeDelete = remove(8, testTreeDelete);
		testTreeDelete = remove(1, testTreeDelete);
		testTreeDelete = remove(4, testTreeDelete);
		testTreeDelete = remove(7, testTreeDelete);
		testTreeDelete = remove(2, testTreeDelete);
		
		testTreeDelete = clear(testTreeDelete);
		printTree(testTreeDelete);
		
		System.out.println();
		System.out.println();
		
		System.out.println(floor(-1, testTree));
		
		
		
		
		TreeSet tree = new TreeSet();
		
		*/
		
		
	}
	

}
