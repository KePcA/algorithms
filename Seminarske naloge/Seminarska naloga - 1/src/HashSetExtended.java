/*
 * HashSetExtended.java - raz�iritev razreda HashSet z dodano metodo floor
 */


import java.util.HashSet;
import java.util.Iterator;



public class HashSetExtended<E> extends HashSet<E> {
	
	
	
	public E floor(E e) {
		
		//Odlo�imo se, da metodo floor uporabimo samo za Integer
		if (e instanceof Integer) {
			
			//Definirano spremenljivko za rezultat
			E result = null;
			
			//Spremenljivka, ki hrani vmesen rezultat, kateri je trenutni najve�ji element
			Integer currentMax = Integer.MIN_VALUE;
			
			//Sprehodimo se skozi elemente
			Iterator<E> iter = this.iterator();
			while(iter.hasNext()) {
				E currentElement = iter.next();
				
				//Naletimo na element, ga vrnemo, ne rabimo ve� iskati
				if(e.equals(currentElement)) {
					result = (E)currentElement;
					break;
				}
				
				//�e je element ve�ji od na�ega nas ne zanima
				if((Integer)e < (Integer)currentElement) {
					continue;
				}
				
				//Element je manj�i ali enak na�emu, zapomnimo si ga, �e je ve�ji od trenutno najve�jega
				if(currentMax <= (Integer)currentElement) {
					currentMax = (Integer)currentElement;
					result = (E)currentMax;
				}
								
			}
			return result;
			
			
		}
		
		//Input ni INTEGER
		else {
			System.out.print("Can only compare Integers");
		}
		return null;
	}
	
	
	
	
	
	public static void main(String args[]) {
		
		
		HashSetExtended<Integer> set = new HashSetExtended<Integer>();
		set.add(3);
		set.add(7);
		set.add(10);
		set.add(2);
		set.add(8);
		set.add(50);
		
		
		System.out.print(set.floor(8));
 		
		

	}
	
	
	
	

}
