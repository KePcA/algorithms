

/*
 * Razred, kjer merimo �asovno zahtevnost operacij za vse �tiri podatkovne strukture
 */

import java.util.Iterator;
import java.util.Random;
import java.util.HashSet;
import java.util.TreeSet;





public class TimeComplexity {
	
	
	
	static final int repetitions = 1;
	
	
	
	public static void main(String[] args) {
		
		
		final int size = 100000;
		
		
		//DEFINIRAMO SPREMENLJIVKE
		long avgTimeAdd = 0;
		long avgTimeContains_contained = 0;
		long avgTimeContains_notContained = 0;
		long avgTimeFloor_contained = 0;
		long avgTimeFloor_notContained = 0;
		
		for(int i=0; i<repetitions; i++) {
			avgTimeAdd += addTime_HSE(size);
			avgTimeContains_contained += containsTime_contained_HSE(size);
			avgTimeContains_notContained += containsTime_notContained_HSE(size);
			avgTimeFloor_contained += floorTime_contained_HSE(size);
			avgTimeFloor_notContained += floorTime_notContained_HSE(size);
		}
		
		avgTimeAdd /= repetitions;
		avgTimeContains_contained /= repetitions;
		avgTimeContains_notContained /= repetitions;
		avgTimeFloor_contained /= repetitions;
		avgTimeFloor_notContained /= repetitions;
		
		System.out.println("ADD: "+avgTimeAdd);
		System.out.println("CONTAINS - YES: "+avgTimeContains_contained);
		System.out.println("CONTAINS - NO: "+avgTimeContains_notContained);
		System.out.println("FLOOR - YES: "+avgTimeFloor_contained);
		System.out.println("FLOOR - NO: "+avgTimeFloor_notContained);
		
		
		
		
		
		
		/*
		System.out.println(addTime_LLE(100000));
		
		System.out.println(containsTime_contained_LLE(100000));
		
		System.out.println(containsTime_notContained_LLE(100000));
		
		System.out.println(floorTime_contained_LLE(100000));
		
		System.out.println(floorTime_notContained_LLE(100000));	
		*/
		
		/*
		
		System.out.println(addTime_HSE(100000));
		
		System.out.println(containsTime_contained_HSE(100000));
		
		System.out.println(containsTime_notContained_HSE(100000));
		
		System.out.println(floorTime_contained_HSE(100000));
		
		System.out.println(floorTime_notContained_HSE(100000));
		
		*/
	}
	
	
	
	//VRNE MNO�ICO RAZLI�NIH ELEMENTOV, VELIKOSTI SIZE
	public static HashSet<Integer> generateTestSet(int size) {
		
		HashSet<Integer> set = new HashSet<Integer>();
		Random rand = new Random();
		while(set.size() < size) {
			set.add(rand.nextInt(Integer.MAX_VALUE) + 1);
		}
		return set;
	}
	
	
	
	//VRNE UNIKATNO MNO�ICO, KI IMA VSE RAZLI�NE ELEMENTE OD MNO�ICE SET - PRESEK JE PRAZNA MNO�ICA
	public static HashSet<Integer> generateTestSet_unique(HashSet<Integer> set) {
			
		HashSet<Integer> newSet = new HashSet<Integer>();
		Random rand = new Random();
		while(newSet.size() < set.size()) {
			int element = rand.nextInt(Integer.MAX_VALUE) + 1;
			if (set.contains(element)) { 
				continue;
			}
			newSet.add(element);
		}
		return newSet;
	}
	
	
	
	
	
	
	
	/*
	 * 
	 *     ANALIZA ZA BBTREE
	 */
	
	
	//VRNE BB DREVO ZGRAJENO IZ MNO�ICE SET
	public static BBTree generateTestTree(HashSet<Integer> set) {
		
		BBTreeMain.initGlobalVariables();
		BBTree tree = BBTreeMain.bottom;
		Iterator<Integer> iter = set.iterator();
		
		while(iter.hasNext()) {
			tree = BBTreeMain.add(iter.next(), tree);
		}
		
		return tree;
	}
	
	
	
	
	
	
	//IZRA�UN �ASA IZVAJANJA ZA METODO ADD - BB DREVO
	public static long addTime_BBTree(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		BBTreeMain.initGlobalVariables();
		BBTree tree = BBTreeMain.bottom;
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree = BBTreeMain.add(iter.next(), tree);
		}
		
		return (System.currentTimeMillis() - startTime);
		
		
	}
	
	
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NAHAJAJO V BB DREVESU
	public static long containsTime_contained_BBTree(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		BBTree tree = generateTestTree(set);
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			BBTreeMain.contains(iter.next(), tree);
		}
		
		return (System.currentTimeMillis() - startTime);
		
	}
	
	
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V BB DREVESU
	public static long containsTime_notContained_BBTree(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		BBTree tree = generateTestTree(set);
		
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			BBTreeMain.contains(iter.next(), tree);
		}
		
		return (System.currentTimeMillis() - startTime);
		
	}
	
	
	
	//IZRA�UN �ASA ZA IZVAJANJE FLOOR V PRIMERU, DA SE ELEMENTI NAHAJAJO V BB DREVESU
	public static long floorTime_contained_BBTree(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		BBTree tree = generateTestTree(set);
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			BBTreeMain.floor(iter.next(), tree);
		}
		
		return (System.currentTimeMillis() - startTime);
			
	}
		
		
	//IZRA�UN �ASA ZA IZVAJANJE METODE FLOOR V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V BB DREVESU
	public static long floorTime_notContained_BBTree(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		BBTree tree = generateTestTree(set);
		
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
			
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			BBTreeMain.floor(iter.next(), tree);
		}
		
		return (System.currentTimeMillis() - startTime);
			
	}
		
		
		
		
		
		
		
		
		
//_____________________________________________________________________________________________________________//
		
		
		
		
		/*
		 * 
		 * ANALIZA ZA LINKEDLIST
		 */
		
		
		
	//VRNE LINKEDLIST ZGRAJEN IZ MNO�ICE SET
	public static LinkedListExtended<Integer> generateTestLLE(HashSet<Integer> set) {
		
		LinkedListExtended<Integer> list = new LinkedListExtended<Integer>();
		Iterator<Integer> iter = set.iterator();
		
		while(iter.hasNext()) {
			list.add(iter.next());
		}
		
		return list;
	}
		
		
		
		
		
		
	//IZRA�UN �ASA IZVAJANJA ZA METODO ADD - LINKEDLIST
	public static long addTime_LLE(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		LinkedListExtended<Integer> list = new LinkedListExtended<Integer>();
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			list.add(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
		
		
	}
		
		
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NAHAJAJO V LINKEDLISTU
	public static long containsTime_contained_LLE(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		LinkedListExtended<Integer> list = generateTestLLE(set);
		Iterator<Integer> iter = set.iterator();
			
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			list.contains(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
			
	}
		
		
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V LINKEDLISTU
	public static long containsTime_notContained_LLE(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		LinkedListExtended<Integer> list = generateTestLLE(set);
			
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
			
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			list.contains(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
			
	}
		
		
		
	//IZRA�UN �ASA ZA IZVAJANJE FLOOR V PRIMERU, DA SE ELEMENTI NAHAJAJO V LINKEDLISTU
	public static long floorTime_contained_LLE(int size) {
				
		HashSet<Integer> set = generateTestSet(size);
		LinkedListExtended<Integer> list = generateTestLLE(set);
		Iterator<Integer> iter = set.iterator();
				
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			list.floor(iter.next());
		}
				
		return (System.currentTimeMillis() - startTime);
				
	}
			
			
	//IZRA�UN �ASA ZA IZVAJANJE METODE FLOOR V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V LINKEDLISTU
	public static long floorTime_notContained_LLE(int size) {
				
		HashSet<Integer> set = generateTestSet(size);
		LinkedListExtended<Integer> list = generateTestLLE(set);
				
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
				
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			list.floor(iter.next());
		}
				
		return (System.currentTimeMillis() - startTime);
				
	}
			
			
			
			
			
			
			
			
			
			
			
			
//_____________________________________________________________________________________________________________//
			
			
			
			
			/*
			 * 
			 * ANALIZA ZA HASHSET
			 */
			
			
			
	//VRNE HASHSET ZGRAJEN IZ MNO�ICE SET
	public static HashSetExtended<Integer> generateTestHSE(HashSet<Integer> set) {
				
		HashSetExtended<Integer> hash = new HashSetExtended<Integer>();
		Iterator<Integer> iter = set.iterator();
				
		while(iter.hasNext()) {
			hash.add(iter.next());
		}
				
		return hash;
	}
			
			
			
			
			
			
	//IZRA�UN �ASA IZVAJANJA ZA METODO ADD - HASHSET
	public static long addTime_HSE(int size) {
				
		HashSet<Integer> set = generateTestSet(size);
		HashSetExtended<Integer> hash = new HashSetExtended<Integer>();
		Iterator<Integer> iter = set.iterator();
				
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			hash.add(iter.next());
		}
				
		return (System.currentTimeMillis() - startTime);
				
				
	}
			
			
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NAHAJAJO V HASHSET
	public static long containsTime_contained_HSE(int size) {
				
		HashSet<Integer> set = generateTestSet(size);
		HashSetExtended<Integer> hash = generateTestHSE(set);
		Iterator<Integer> iter = set.iterator();
				
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			hash.contains(iter.next());
		}
				
		return (System.currentTimeMillis() - startTime);
				
	}
			
			
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V HASHSET
	public static long containsTime_notContained_HSE(int size) {
				
		HashSet<Integer> set = generateTestSet(size);
		HashSetExtended<Integer> hash = generateTestHSE(set);
				
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
				
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			hash.contains(iter.next());
		}
				
		return (System.currentTimeMillis() - startTime);
				
	}
			
			
			
	//IZRA�UN �ASA ZA IZVAJANJE FLOOR V PRIMERU, DA SE ELEMENTI NAHAJAJO V HASHSET
	public static long floorTime_contained_HSE(int size) {
					
		HashSet<Integer> set = generateTestSet(size);
		HashSetExtended<Integer> hash = generateTestHSE(set);
		Iterator<Integer> iter = set.iterator();
					
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			hash.floor(iter.next());
		}
					
		return (System.currentTimeMillis() - startTime);
					
	}
				
				
	//IZRA�UN �ASA ZA IZVAJANJE METODE FLOOR V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V HASHSET
	public static long floorTime_notContained_HSE(int size) {
					
		HashSet<Integer> set = generateTestSet(size);
		HashSetExtended<Integer> hash = generateTestHSE(set);
					
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
					
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			hash.floor(iter.next());
		}
					
		return (System.currentTimeMillis() - startTime);
					
	}
		
		
		
		
		
		
		
		
				
				
				
				
				
	//_____________________________________________________________________________________________________________//
	
	
	
	
	/*
	 * 
	 * ANALIZA ZA TREESET
	 */
	
	
	
	//VRNE  TREESET ZGRAJEN IZ MNO�ICE SET
	public static TreeSet<Integer> generateTestTS(HashSet<Integer> set) {
		
		TreeSet<Integer> tree = new TreeSet<Integer>();
		Iterator<Integer> iter = set.iterator();
		
		while(iter.hasNext()) {
			tree.add(iter.next());
		}
		
		return tree;
	}
	
	
	
	
	
	
	//IZRA�UN �ASA IZVAJANJA ZA METODO ADD - TREESET
	public static long addTime_TS(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		TreeSet<Integer> tree = new TreeSet<Integer>();
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree.add(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
		
		
	}
	
	
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NAHAJAJO V TREESET
	public static long containsTime_contained_TS(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		TreeSet<Integer> tree = generateTestTS(set);
		Iterator<Integer> iter = set.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree.contains(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
		
	}
	
	
	//IZRA�UN �ASA ZA IZVAJANJE METODE CONTAINS V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V TREESET
	public static long containsTime_notContained_TS(int size) {
		
		HashSet<Integer> set = generateTestSet(size);
		TreeSet<Integer> tree = generateTestTS(set);
		
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
		
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree.contains(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
		
	}
	
	
	
	//IZRA�UN �ASA ZA IZVAJANJE FLOOR V PRIMERU, DA SE ELEMENTI NAHAJAJO V TREESET
	public static long floorTime_contained_TS(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		TreeSet<Integer> tree = generateTestTS(set);
		Iterator<Integer> iter = set.iterator();
			
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree.floor(iter.next());
		}
		
		return (System.currentTimeMillis() - startTime);
			
	}
		
		
	//IZRA�UN �ASA ZA IZVAJANJE METODE FLOOR V PRIMERU, DA SE ELEMENTI NE NAHAJAJO V TREESET
	public static long floorTime_notContained_TS(int size) {
			
		HashSet<Integer> set = generateTestSet(size);
		TreeSet<Integer> tree = generateTestTS(set);
			
		HashSet<Integer> testSet = generateTestSet_unique(set);
		Iterator<Integer> iter = testSet.iterator();
			
		long startTime = System.currentTimeMillis();
		while(iter.hasNext()) {
			tree.floor(iter.next());
		}
			
		return (System.currentTimeMillis() - startTime);
			
	}
				
				

			
			
		
	
}
