import java.util.Random;

public class SkipListALG implements Sem2Interface {
	
	
	public String key;
	public SkipListALG[] pointers;

	
	
	
	//Initialize empty skiplist with maxLevel 32
	public SkipListALG() {
		key = null;
		pointers = new SkipListALG[32];
		for(int i=0; i<pointers.length; i++) {
			pointers[i] = null;
		}
	}
	
	
	//New node with given level and value of s
	public SkipListALG(int level, String s) {
		key = s;
		pointers = new SkipListALG[level];
	}
	
	
	
	//Return random value for random variable X ~ Geo(0.5)
	public int randomLevel() {
		Random random = new Random();
		int maxLevel = pointers.length;
		int lvl = 1;
		while (random.nextFloat() < 0.5 && lvl < maxLevel-1) {
			lvl++;
		}
		return lvl;
	}
	
	
	
	
	// returns true if element is in list (set), false if not - O(logn)
	public boolean contains(String s) {
		SkipListALG x = this;	
		
		//Looping through all levels
		for(int i=pointers.length-1; i>=0; i--) {		
			
			//At level i, move through the list as long as key is smaller
			while(x.pointers[i] !=null && x.pointers[i].key.compareTo(s) < 0) {
				x = x.pointers[i];	
			}		
		}		
		
		//Currently in front of over element - move
		x = x.pointers[0];		
		
		//If list contains string s, then it has to be x, otherwise it does not contain s
		if (x!=null && x.key.equals(s)) {
			return true;
		}
		else {
			return false;
		}

	}
    
	
	
	
	
	
	// if element not in list it adds it and returns true; otherwise returns false - O(logn)
	public boolean add(String s) {
		SkipListALG[] update = new SkipListALG[pointers.length];
		SkipListALG x = this;
		
		//Looping through all levels
		for(int i=pointers.length-1; i>=0; i--) {
			
			//At level i, move through the list as long as key is smaller
			while(x.pointers[i] != null && x.pointers[i].key.compareTo(s) < 0) {
				x = x.pointers[i];
			}
			
			//Save the last element which had smaller key than s - to be able to correctly adjust pointers
			update[i] = x;
		}
		
		//Currently in front of over element - move
		x = x.pointers[0];
		
		//If list contains string s, then return false - element is already in the list
		if (x!=null && x.key.equals(s)) {
			return false;
		}
		
		//Element is not in the list
		else {
			
			//generate random level for the node
			int lvl = randomLevel();
			
			//create new node with randomly generated level and key s
			x = new SkipListALG(lvl, s);
			
			//Correctly adjust pointers
			for(int i=0; i<lvl; i++) {
				x.pointers[i] = update[i].pointers[i];
				update[i].pointers[i] = x;
			}
		}
		return true;
	}
	
	
	
	
	
	
	// remove all elements between a and b (inclusive) from the list; a and b do not have to be elements from the list - O(logn)
	public void removeBetween(String a, String b) {
		
		if(a.equals(b)) {
			remove(b);
		}
		
		// �e je a > b, potem nimamo kaj brisati
		else if(a.compareTo(b) < 0) {
			
			//Najdemo ustrezen element (enako kot pri contains) - glede na a
			SkipListALG[] update_x = new SkipListALG[pointers.length];
			SkipListALG x = this;
			for(int i=pointers.length-1; i>=0; i--) {
				while(x.pointers[i] !=null && x.pointers[i].key.compareTo(a) < 0) {
					x = x.pointers[i];
				}
				update_x[i] = x;
				
			}
			x = x.pointers[0];
			
			//Tako kot za x, naredimo �e za y - glede na b le da tu ne potrebujemo polja update
			SkipListALG y = this;
			for(int i=pointers.length-1; i>=0; i--) {
				while(y.pointers[i] != null && y.pointers[i].key.compareTo(b) < 0) {
					y = y.pointers[i];
				}		
			}
			y = y.pointers[0];
			
			//�e je x null, potem so vsi elementi manj�i od a - nimamo kaj brisati
			if (x!=null) {
				for(int i=0; i<pointers.length; i++) {
					
					//�e �e ka�e na null, smo kon�ali, vsi od tega indeksa naprej tudi ka�ejo na null in nimamo ve� kaj prevezovati
					if(update_x[i].pointers[i] == null) {
						break;
					}
					
					//Element na tem nivoju ka�e na x - zve�i z y, �e lahko
					else if(update_x[i].pointers[i].equals(x)) {
						
						//�e y vsebuje nivo i, potem zve�i s tistim kamor ka�e na i-tem nivoju
						if(y==null) {
							update_x[i].pointers[i] = null;
						}
						else if(y != null && i < y.pointers.length) {
							update_x[i].pointers[i] = y.pointers[i];
						}
						//i > y.pointers.lenght - najdi element, na katerega moramo vezati
						else {
							while(x.pointers[i] != null && x.pointers[i].key.compareTo(y.key) < 0) {
								x = x.pointers[i];
							}
							update_x[i].pointers[i] = x.pointers[i];
						}
					}
					
					//Ne ka�e na x in tudi ne na null, torej smo na vi�jem nivoju, kot jih ima x in stojimo to�no pred x (�e bi bili na ni�jem, bi zagotovo bil enak x)
					else if(!update_x[i].pointers[i].equals(x)) {
						
						//�e y vsebuje nivo i, potem zve�i s tistim kamor ka�e na pri i-tem nivoju
						if(y==null) {
							update_x[i].pointers[i] = null;
						}
						else if(y != null && i < y.pointers.length) {
							update_x[i].pointers[i] = y.pointers[i];
						}
						//i > y.pointers.lenght - najdi element, ki na katerega moramo vezati
						else {
							SkipListALG nov = update_x[i];
							while(nov.pointers[i] != null && nov.pointers[i].key.compareTo(y.key) < 0) {
								nov = nov.pointers[i];
							}
							update_x[i].pointers[i] = nov.pointers[i];
						}
					}
				
				}
				
			}
		}
			
	}
	
	
	
	
	// removes element and returns true, returns false if element not in list - O(logn)
	public boolean remove(String s) {
		
		//Podobno kot contains, najdemo ustrezen element
		SkipListALG[] update = new SkipListALG[pointers.length];
		SkipListALG x = this;
		for(int i=pointers.length-1; i>=0; i--) {

			while(x.pointers[i] !=null && x.pointers[i].key.compareTo(s) < 0) {
				x = x.pointers[i];	
			}
			update[i] = x;
			
		}
		x = x.pointers[0];
		
		//�e se element nahaja, potem je to x in ga izbri�emo, sicer ga ni in ga ne moremo izbrisato
		if (x!=null && x.key.equals(s)) {
			
			for(int i=0; i<pointers.length; i++) {
				//�e update ka�e na null oz. na nek element ki ni x, potem smo na vi�jem nivoju kot je x in smo kon�ali s prevezovanjem(ostali nivoji so ok)
				if(update[i].pointers[i]==null || !update[i].pointers[i].equals(x)) {
					break;
				}
				
				//Preve�emo
				update[i].pointers[i] = x.pointers[i];
			}
			return true;
		}
		else {
			return false;
		}
		
	}
	
	
	
	
	// removes all elements from list - O(1)
	public void clear() {
		key = null;
		for(int i=0; i<pointers.length; i++) {
			pointers[i] = null;
		}
		 
	}
	
	
	
	// returns all elements from list in a String array (ordered) O(n)
	public String[] toArray() {
		
		//First, count how many elements are in the list
		int len = 0;
		SkipListALG x = this;
		while(x.pointers[0]!=null) {
			x = x.pointers[0];
			len++;
		}
		
		//Create array and save elements of the list into an array
		String[] array = new String[len];
		x = this;
		len = 0;
		while(x.pointers[0]!=null) {
			x = x.pointers[0];
			array[len] = x.key;
			len++;
		}
		
		return array;
	}
    
	
	
	// returns your student ID
	public String studentId() {
		return "27132032";
	}
	
	
	
	/*
	
	public void printElements() {
		SkipListALG x = this;
		while(x.pointers[0]!=null) {
			x = x.pointers[0];
			System.out.print(x.key+" ");
		}
		System.out.println("Fin");
	}
	
	
	
	public void allOk() {
		SkipListALG x = this;
		while(x.pointers[0]!=null) {
			x = x.pointers[0];
			for(int i=0; i<x.pointers.length; i++) {
				System.out.print(x.key+"  ");
			}
			System.out.println();
			
		}
		System.out.println();
	}
	*/
	
	

}
