
import java.util.ArrayList;

// Class name and solve/studentId method declarations must remain the same! 
// You may import any java.* library. 
public class Sem3ALG {
	
	
	
    // You may (will have to) program additional Classes, etc... 
    public static boolean solve(String[] grammar, String word) // You may assume that your method will be tested in the following setting: 
    // - grammar will contain between 1 and 100 strings, 
    // - each string will represent one production (possibly with multiple right 
    // hand sides, separated by | (see examples)), 
    // - word can be empty or up to 10000 terminal symbols (characters) long. 
    {
        // Your solution. 
        final int wLenght = word.length();
        final int gLenght = grammar.length;
        final String[] neterminali = GetNeterminale(grammar);
        
        boolean[][][] produkcija = new boolean[wLenght][wLenght][gLenght];
        
		if(wLenght == 1) {
			return UnitProduction(word.charAt(0), grammar, "S");
		}
                
        /**
         * 
         */
        for (int i = 0; i < wLenght; i++) {
            for (int j = 0; j < gLenght; j++) {
                produkcija[i][0][j] = UnitProduction(word.charAt(i), grammar, neterminali[j]);
            }
        }
        String[] neterminalGrammer = GetNonTerminalProductionSet(grammar);
        
        for (int i = 1; i < wLenght; i++) {
            for (int j = 0; j < wLenght-i; j++) {
                for (int k = 0; k < i; k++) {
                    for (int l = 0; l < neterminalGrammer.length; l++) {
                        
                        int prvi = getIndexOfNonTerminal(neterminali, neterminalGrammer[l].charAt(0));
                        int drugi = getIndexOfNonTerminal(neterminali, neterminalGrammer[l].charAt(1));
                        int tretji = getIndexOfNonTerminal(neterminali, neterminalGrammer[l].charAt(2));
                        
                        if(produkcija[j][k][drugi] && produkcija[j+k+1][i-k-1][tretji]){
                            produkcija[j][i][prvi] = true;
                        }
                    }
                }
            }
        }
        
        for (int i = 0; i < neterminali.length; i++) {
            if(produkcija[0][wLenght-1][i])
            {
                return true;
            }
        }
        
        return false;
    }
    
    public static int getIndexOfNonTerminal(String[] nontermnalList, char nonterminal){
        for (int i = 0; i < nontermnalList.length; i++) {
            if(nontermnalList[i].equals(Character.toString(nonterminal)))
                return i;
        }
        return -1;
    }
    
    public static String[] GetNonTerminalProductionSet(String[] grammer){
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < grammer.length ; i++) {
           
            String[] tmp = grammer[i].substring(3).split("\\|");
            
            for (int j = 0; j < tmp.length; j++) {
                if(tmp[j].length()==2){
                    list.add(grammer[i].substring(0,1)+tmp[j]);
                }
            }
        }
        String[] sez = new String[list.size()];
        sez = list.toArray(sez);
        
        return sez;
    }
    
    public static boolean UnitProduction(char znak, String[] grammer, String neterminal){
        for (int i = 0; i < grammer.length; i++) {
            String tmp = grammer[i];
            if(tmp.substring(0, 1).equals(neterminal)){
                for (int j = 0; j < tmp.length(); j++) {
                    if(tmp.charAt(j) == znak){
                       return true; 
                    }
                }
            }
        }
        return false;
    }
    
    public static String[] GetNeterminale(String[] grammer){
        String[] seznam = new String[grammer.length];
        for (int i = 0; i < grammer.length; i++) {
            if(grammer[i]!= null && grammer[i]!="")
                seznam[i] = grammer[i].substring(0, 1);
        }
        return seznam;
        
    }
    
/**
 * Inicializacija seznama rezultatov produkcije
 * @param wordLenght - dolžina vnesene besede
 * @param grammerLenght - golžina seznama slovnice
 * @return seznam logičnih vrednosti, kjer so vse vrednosti nastavljene na false
 */
    public static boolean[][][] initializeList(int wordLenght,int grammerLenght){
        boolean[][][] list = new boolean[wordLenght][wordLenght][grammerLenght];
        for (int i = 0; i < wordLenght; i++) {
            for (int j = 0; j < wordLenght; j++) {
                for (int k = 0; k < grammerLenght; k++) {
                    list[i][j][k] = false;
                }
            }
        }
        return list;
    }
    
    public static String studentId() {
        return "63080034";
    }
}