import java.util.LinkedList;

public class Sem3ALG {

	
	public static void main(String[] args) {
		

		String grammar1 = "S->AA;A->BB|CC|DD|EE|FF|GG|d|e";
		String grammar2 = "S->a";
		String grammar3 = "S->~";
		
		String word = "aa";
		System.out.println(word.length());
		
		
		
		System.out.print(solve(grammar1.split(";"), word));


				
		
	}


	
	public static boolean solve(String[] grammar, String word) {
		
		//Defininf all arrays we need
		String[] nonTerminals = nonTerminals(grammar);
		String[] input = toArrayOfStrings(word);
		String[] unitProductions = unitProductions(grammar);
		String[] nonTerminalProductions = nonTerminalProductions(grammar);
		
		//Only one character - in grammar only in case S->input
		if(input.length==1) {
			String startProduction = "S->"+input[0];
			if(contains(unitProductions, startProduction)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			
			//Result array
			boolean[][][] bools = new boolean[input.length][input.length][nonTerminals.length];
			
			//For each production nonTerninal -> Terminal we set bools[i][0][j] to true
			for(int i=0; i<input.length; i++) {
				for(int j=0; j<nonTerminals.length; j++) {
					String production = nonTerminals[j] + "->" + input[i];
					if(contains(unitProductions, production)) {
						bools[i][0][j] = true;
					}
				}
			}
			
			
			for(int i=1; i<input.length; i++) {
				for(int j=0; j<input.length-i; j++) {
					for(int k=0; k<i; k++) {
						for(int l=0; l<nonTerminalProductions.length; l++) {
							
							//For each production nonTermA -> nonTermB, nonTermC set bools[j][i][index_A] to true if both
							// bools[j][k][index_B] and bools[j+k+1][i-k-1][indexC] are true
							String nonTerminalProduction = nonTerminalProductions[l];
							String[] tempSplit = nonTerminalProduction.split("->");
							String nonTerminal_A = tempSplit[0];
							String nonTerminal_B = "" + tempSplit[1].charAt(0);
							String nonTerminal_C = "" + tempSplit[1].charAt(1);
							
							int indexA = indexOfNonTerminal(nonTerminals, nonTerminal_A);
							int indexB = indexOfNonTerminal(nonTerminals, nonTerminal_B);
							int indexC = indexOfNonTerminal(nonTerminals, nonTerminal_C);
							
							if (bools[j][k][indexB] && bools[j+k+1][i-k-1][indexC]) {
								bools[j][i][indexA] = true;
							}
													
						}
					}
				}
			}
			
			//Return true of any of following is true, meaning that we found one way to produce particuar string
			for(int i=0; i<nonTerminals.length; i++) {
				if (bools[0][input.length-1][i]) {
					return true;
				}
			}
					
			return false;
		}
	}
	
	
	
	//Returns all non-terminals in grammar
	public static String[] nonTerminals(String[] grammar) {
		LinkedList<String> nonTerminals = new LinkedList<String>();
		for(int i=0; i<grammar.length; i++) {
			String nonTerminal = "" + grammar[i].charAt(0);
			if (!nonTerminals.contains(nonTerminal)) {
				nonTerminals.add(nonTerminal);
			}
		}
		return nonTerminals.toArray(new String[nonTerminals.size()]);
	}
	
	
	//Prints array
	public static void printArray(String[] array) {
		for(int i=0; i<array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}
	
	
	//From word makes an array
	public static String[] toArrayOfStrings(String word) {
		String[] input = new String[word.length()];
		for(int i=0; i<word.length(); i++) {
			input[i] = "" + word.charAt(i);
		}
		return input;
	}
	
	
	//Returns all possible unit productions from not-terminals to sigle unit on one step
	public static String[] unitProductions(String[] grammar) {
		LinkedList<String> unitProductions = new LinkedList<String>();
		for(int i=0; i<grammar.length; i++) {
			String[] tempSplit = grammar[i].split("->");
			String nonTerminal = tempSplit[0];
			String rest = tempSplit[1];
			for(int j=0; j<rest.length(); j++) {
				String character = "" + rest.charAt(j);
				if (isLowercaseLetter(character)) {
					String unitProduction = nonTerminal + "->" + character;
					unitProductions.add(unitProduction);
				}
			}
		}		
		return unitProductions.toArray(new String[unitProductions.size()]);			
	}
	
	
	//Returns all possible non-terminal productions from not-terminals to single non-terminal in one step (two possibilities always)
	public static String[] nonTerminalProductions(String[] grammar) {
		LinkedList<String> nonTerminalProductions = new LinkedList<String>();
		for(int i=0; i<grammar.length; i++) {
			String[] tempSplit = grammar[i].split("->");
			String nonTerminal = tempSplit[0];
			String rest = tempSplit[1];
			String[] productions = rest.split("\\|");
			for(int j=0; j<productions.length; j++) {
				if (!isLowercaseLetter(productions[j])) {
					String nonTerminalProduction = nonTerminal + "->" + productions[j];
					nonTerminalProductions.add(nonTerminalProduction);
				}
			}
			
		}		
		return nonTerminalProductions.toArray(new String[nonTerminalProductions.size()]);
	}
	
	
	//Checks if character is lower case letter or ~
	public static boolean isLowercaseLetter(String character) {
		String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "q", "s", "t", "u", "v", "w", "z", "y", "x", "~"};
		for(int i=0; i<letters.length; i++) {
			if (character.equals(letters[i])) {
				return true;
			}
		}
		return false;
	}
	
	
	//True, if input is in array
	public static boolean contains(String[] array, String input) {
		for(int i=0; i<array.length; i++) {
			if (input.equals(array[i])) {
				return true;
			}
		}
		return false;
	}
	
	
	
	//Return index of array of non-terminal, if array contains it
	public static int indexOfNonTerminal(String[] nonTerminals, String nonTerminal) {
		for(int i=0; i<nonTerminals.length; i++) {
			if(nonTerminal.equals(nonTerminals[i])) {
				return i;
			}
		}
		return -1;
	}
		
	
	
	
	public static String studentId() {
		return "27132032";
	}
	
	
	
	
	
	
}
