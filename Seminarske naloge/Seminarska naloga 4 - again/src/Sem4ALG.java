import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

class ColumnComparator implements Comparator<Double[]> {
	
	@Override
	public int compare(Double[] first, Double[] second) {
		
		return Double.compare(first[0], second[0]);
	}
}

public class Sem4ALG {
	
	
	public static void main(String[] args) {
		
		
		int[] x1 = {-1, 3, 2, 2, 4, 0, 4, 2, 5};
		int[] y1 = {-1, 1, 3, 4, 3, 3, 2, 2, 3};
		
		/*
		
		Point[] convexHull1 = convexHull(x1, y1);
		printPoints(convexHull1);
		
		int[] x2 = {2, 4, 2};
		int[] y2 = {3, 3, 2};
		
		
		Point[] convexHull2 = convexHull(x2, y2);
		printPoints(convexHull2);
		*/
		
		int[] x3 = {3, 1, -3, -3, -3};
		int[] y3 = {0, -3, 1, -3, 0};
		
		System.out.println(solve(x1,y1));
		
	}
	
	
	public static int solve(int[] x, int[] y) {
		
		int layers = 0;
		while(true) {
			int[][] remaining = convexHull(x,y);
			layers++;
			if(remaining.length==0) {
				break;
			}
			x = new int[remaining.length];
			y = new int[remaining.length];
			for(int i=0; i<remaining.length; i++) {
				x[i] = remaining[i][0];
				y[i] = remaining[i][1];
			}
		}
		
		
		return layers;
	}
	
	
	
	public static int[][] convexHull(int[] x, int[] y) {
		
		int N = x.length;
		int[][] points = new int[N+1][2];
		
		//Get index with smallest Y and copy points into array
		int indexSmallestY = 0;
		int currentMin = y[indexSmallestY];
		for(int i=1; i < N; i++) {
			if(y[i] < currentMin) {
				currentMin = y[i];
				indexSmallestY = i;
			}
			points[i][0] = x[i-1];
			points[i][1] = y[i-1];
		}
		indexSmallestY++;
		points[N][0] = x[N-1];
		points[N][1] = y[N-1];
		
		//Replace with smallest
		int tempX_1 = points[1][0];
		int tempY_1 = points[1][1];
		points[1][0] = points[indexSmallestY][0];
		points[1][1] = points[indexSmallestY][1];
		points[indexSmallestY][0] = tempX_1;
		points[indexSmallestY][1] = tempY_1;
		
		
		Double[][] polarAngles = new Double[N-1][2];
		int point1_x = points[1][0];
		int point1_y = points[1][1];
		for(int i=2; i < N+1; i++) {
			int point2_x = points[i][0];
			int point2_y = points[i][1];
			double angle = Math.atan2(point2_y - point1_y, point2_x - point1_x);
			polarAngles[i-2][0] = angle;
			polarAngles[i-2][1] = (double)i;
		}
		
		Arrays.sort(polarAngles, new ColumnComparator());
		
		int[][] tempPoints = new int[N+1][];
		for(int i=0; i < N+1; i++) {
			tempPoints[i] = points[i].clone();
		}
		for(int i=0; i < N-1; i++) {
			points[i+2][0] = tempPoints[polarAngles[i][1].intValue()][0];
			points[i+2][1] = tempPoints[polarAngles[i][1].intValue()][1];
		}
		
		points[0][0] = points[N][0];
		points[0][1] = points[N][1];
		
		int M = 1;
		for(int i=2; i<=N; i++) {
			while(ccw(points[M-1], points[M], points[i]) <= 0) {
				if(M > 1) {
					M -= 1;
				}
				else if (i == N) {
					break;
				}
				else {
					i += 1;
				}
			}
			M += 1;
			
			int tempX_2 = points[i][0];
			int tempY_2 = points[i][1];
			points[i][0] = points[M][0];
			points[i][1] = points[M][1];
			points[M][0] = tempX_2;
			points[M][1] = tempY_2;
			
			
		}
		
		LinkedList<int[]> resultList = new LinkedList<int[]>();
		for(int i=M+1; i<N+1; i++) {
			int[] point = points[i];
			boolean isOnHull = false;
			for(int j=0; j<M; j++) {
				int[] point1 = points[j];
				int[] point2 = points[j+1];
				if(isPointOnLine(point1, point2, point)) {
					isOnHull = true;
					break;
				}
 			}
			if(!isOnHull) {
				resultList.add(point);
			}
		}
		
		int[][] result = new int[resultList.size()][2];
		for(int i=0; i<result.length; i++) {
			result[i][0] = resultList.get(i)[0];
			result[i][1] = resultList.get(i)[1];
		}
		
		
		return result;
	}
	
	
	public static int ccw(int[] point1, int[] point2, int[] point3) {
		return (point2[0] - point1[0])*(point3[1] - point1[1]) - (point2[1] - point1[1])*(point3[0] - point1[0]);
		
	}
	
	public static boolean isPointOnLine(int[] point1, int[] point2, int[] point) {
		if(point1[0] == point2[0] && point2[0] == point[0]) {
			return true;
		}
		else if(point1[1] == point2[1] && point2[1] == point[1]) {
			return true;
		}
		else {
			return ((double)(point[1] - point1[1])) / (point2[1] - point1[1]) == ((double)(point[0] - point1[0])) / (point2[0] - point1[0]);
		}
	}

}


