import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;


//Class representing point in 2D
class Point {
	
	protected int x;
	protected int y;
	
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	

}


//Class representing polar angle between two points and index of that point in array
class Polar {
	
	
	protected double angle;
	protected int index;
	
	
	public Polar(double angle, int index) {
		this.angle = angle;
		this.index = index;
	}
}

//Comparator for sorting by polar angles
class PolarComparator implements Comparator<Polar> {

	@Override
	public int compare(Polar polar1, Polar polar2) {		
		return Double.compare(polar1.angle, polar2.angle);
	}
	
	
	
}



public class Sem4ALG {
	
	/*
	 * Uporabljen je Graham algoritem. V najslab�em primeru je �asovna zahtevnost O(n^2).
	 * Sortiranje na�eloma poteka z O(nlogn), v najslab�em primeru se lahko zgodi O(n^2), kar je tudi najslab�i
	 * primer �asovne zahtevnosti. V povpre�ju je �asovna zahtevnost tudi O(n^2)
	 */
	
	
	public static int solve(int[] x, int[] y) {
		
		//There are no layers if no point is given
		if(x.length == 0 || y.length==0) {
			return 0;
		}
		
		//For faster procedure of length 1 or 2
		if((x.length == 1 && y.length == 1) || (x.length == 2 && y.length == 2)) {
			return x.length;
		}

		//Remove points on convex hull and count layers
		int layers = 0;
		while(true) {
			Point[] remaining = convexHull(x,y);
			layers++;
			if(remaining.length==0) {
				break;
			}
			x = new int[remaining.length];
			y = new int[remaining.length];
			for(int i=0; i<remaining.length; i++) {
				x[i] = remaining[i].x;
				y[i] = remaining[i].y;
			}
			
		}	
		return layers;
	}
	
	
	
	
	public static Point[] convexHull(int[] x, int[] y) {
		
		int N = x.length;
		Point[] points = arrayOfPoints(x, y);
		
		//Point with lowest y coordinate comes first
		int indexLowestY = indexOfPointWithLowestY(points);
		Point temp = points[1];
		points[1] = points[indexLowestY];
		points[indexLowestY] = temp;
		
		//Get polar angles between points and sort it
		Polar[] polars = polarAngles(points);
		Arrays.sort(polars, new PolarComparator());
		Point[] tempPoints = points.clone();
		for(int i=0; i<N-1; i++) {
			points[i+2] = tempPoints[polars[i].index];
		}
		points[0] = points[N];
		
		//Main stream of algorithm
		int M = 1;
		for(int i=2; i<=N; i++) {
			//Three points are on the same line
			while(ccw(points[M-1], points[M], points[i]) <= 0) {
				if(M > 1) {
					M -= 1;
				}
				else if (i == N) {
					break;
				}
				else {
					i += 1;
				}
			}
			M += 1;
			
			//Point is on the hull, put it in front
			Point temp2 = points[i];
			points[i] = points[M];
			points[M] = temp2;
			
		}
		
		//Get convex hull out of array of points
		Point[] convexHull = new Point[M+1];
		for(int i=0; i<M+1; i++) {
			convexHull[i] = points[i];
		}
		
		//Result is points remaining (inside hull)
		LinkedList<Point> result = new LinkedList<Point>();
		for(int i=M+1; i<N+1; i++ ) {
			//Check for points being on the hull, do not add them to the result
			if(!isOnConvexHull(convexHull, points[i])) {
				result.add(points[i]);
			}
		}
				
		return result.toArray(new Point[result.size()]);
	}
	
	
	//Returns index of points in array with lowest Y coordinate
	public static int indexOfPointWithLowestY(Point[] points) {
		int index = 1;
		Point currentMin = points[index];
		for(int i=2; i<points.length; i++) {
			if(points[i].y < currentMin.y) {
				currentMin = points[i];
				index = i;
			}
		}		
		return index;
	}
	
	
	//Turns array of coordinates x and y into array of points
	public static Point[] arrayOfPoints(int[] x, int[] y) {
		Point[] points = new Point[x.length+1];
		for(int i=1; i<points.length; i++) {
			points[i] = new Point(x[i-1], y[i-1]);
		}
		return points;
	}
	
	
	//Calculates polar angles between points[1] and all the others
	public static Polar[] polarAngles(Point[] points) {
		Polar[] polars = new Polar[points.length-2];
		Point point1 = points[1];
		for(int i=2; i<points.length; i++) {
			Point point2 = points[i];
			polars[i-2] = new Polar(polarAngleBetweenTwoPoints(point1, point2), i);
		}
		return polars;
	}
	
	
	//Returns points inside the convex hull
	public static Point[] insideHull(Point[] points, Point[] pointsOnHull) {
		Point[] result = new Point[points.length - pointsOnHull.length];
		int index = 0;
		for(int i=0; i<points.length; i++) {
			if(!contains(points[i], pointsOnHull)) {
				result[index] = points[i];
				index++;
			}
		}
		return result;
	}
	
	
	//Checks if points is present in array of points
	public static boolean contains(Point point, Point[] points) {
		for(int i=0; i<points.length; i++) {
			if(point.equals(points[i])) {
				return true;
			}
		}
		return false;
	}
	
	
	//Calculates polar angle between two points
	public static double polarAngleBetweenTwoPoints(Point p1, Point p2) {
		return Math.atan2(p2.y - p1.y, p2.x - p1.x);
	}
	
	
	//Calculates orientation of three points
	public static int ccw(Point p1, Point p2, Point p3) {
		return (p2.x - p1.x)*(p3.y - p1.y) - (p2.y - p1.y)*(p3.x - p1.x);
	}
	
	//Checks if point is in convex hull
	public static boolean isOnConvexHull(Point[] convexHull, Point point) {
		for(int i=0; i<convexHull.length-1; i++) {
			Point point1 = convexHull[i];
			Point point2 = convexHull[i+1];
			if(isPointOnLine(point1, point2, point)) {
				return true;
			}
			
		}
		return false;
	}
	
	//Checks if point is on the line of point1 and point2
	public static boolean isPointOnLine(Point point1, Point point2, Point point) {
		if(point1.x == point2.x && point2.x == point.x) {
			return true;
		}
		else if(point1.y == point2.y && point2.y == point.y) {
			return true;
		}
		else {
			return ((double)(point.y - point1.y)) / (point2.y - point1.y) == ((double)(point.x - point1.x)) / (point2.x - point1.x);
		}
	}
	
	
	//Print points in the array
	public static void printPoints(Point[] points) {
		for(int i=0; i<points.length; i++) {
			Point point = points[i];
			System.out.print("("+point.x+","+point.y+") ");
		}
		System.out.println();
	}
	
	
	
	public static String studentId() {
		return "27132032";
	}

}
